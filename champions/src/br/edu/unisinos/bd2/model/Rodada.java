package br.edu.unisinos.bd2.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Rodada implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "s_Rodada")
	@SequenceGenerator(name = "s_Rodada", sequenceName = "s_Rodada", allocationSize = 1)
	@Column(length = 2, name = "id")
	private Long idRodada;
	
	@Column(length = 2, name = "total_jogos")
	private int totalJogos;
	
	@Column(length = 3, name = "total_amarelos")
	private int totalAmarelos;
	
	@Column(length = 3, name = "total_vermelhos")
	private int totalVermelhos;
	
	@Column(length = 3, name = "total_gols")
	private int totalGols;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "id_campeonato", foreignKey = @ForeignKey(name = "fk_rodada_campeonato"))
	private Campeonato campeonato;
	
	@OneToMany(mappedBy = "rodadaJogo")
	private List<Jogo> jogos;
	
	@OneToMany(mappedBy = "rodadaClassificacao")
	private List<ClassificacaoRodada> classificacoesRodada;
	
	
}
