package br.edu.unisinos.bd2.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Jogador implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "s_Jogador")
	@SequenceGenerator(name = "s_Jogador", sequenceName = "s_Jogador", allocationSize = 1)
	@Column(name = "id")
	private Long idJogador;
	
	@Column(length = 50, nullable = false)
	private String nome;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "id_clube", foreignKey = @ForeignKey(name = "fk_jogador_clube"))
	private Clube clube;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "id_posicao", foreignKey = @ForeignKey(name = "fk_jogador_posicao"))
	private Posicao posicao;
	
}
