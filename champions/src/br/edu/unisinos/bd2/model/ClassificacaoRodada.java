package br.edu.unisinos.bd2.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity(name = "classificacao_rodada")
public class ClassificacaoRodada implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "s_ClassificacaoRodada")
	@SequenceGenerator(name = "s_ClassificacaoRodada", sequenceName = "s_ClassificacaoRodada", allocationSize = 1)
	@Column(name = "id")
	private Long idClassificacaoRodada;
	
	@Column(length = 2, nullable = false, unique = true)
	private int posicao;
	
	@Column(length = 3, name = "total_pontos")
	private int totalPontos;
	
	@Column(length = 2, name = "total_vitorias")
	private int totalVitorias;
	
	@Column(length = 2, name = "total_empates")
	private int totalEmpates;
	
	@Column(length = 2, name = "total_derrotas")
	private int totalDerrotas;
	
	@Column(length = 3, name = "total_gols_pro")
	private int totalGolsPro;
	
	@Column(length = 3, name = "total_gols_contra")
	private int totalGolsContra;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "id_rodada", foreignKey = @ForeignKey(name = "fk_classificacao_rodada_rodada"))
	private Rodada rodadaClassificacao;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "id_clube", foreignKey = @ForeignKey(name = "fk_jogo_clube"))
	private Clube clube;
	
}
