package br.edu.unisinos.bd2.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Jogo implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "s_Jogo")
	@SequenceGenerator(name = "s_Jogo", sequenceName = "s_Jogo", allocationSize = 1)
	@Column(name = "id")
	private Long idJogo;
	
	@Column(length = 2, name = "clube1_gol")
	private int clube1Gol;
	
	@Column(length = 2, name = "clube2_gol")
	private int clube2Gol;
	
	@Column(length = 2, name = "clube1_amarelo")
	private int clube1Amarelo;
	
	@Column(length = 2, name = "clube2_amarelo")
	private int clube2Amarelo;
	
	@Column(length = 2, name = "clube1_vermelho")
	private int clube1Vermelho;
	
	@Column(length = 2, name = "clube2_vermelho")
	private int clube2Vermelho;
	
	@Column(length = 1, name = "clube1_ponto")
	private int clube1Ponto;
	
	@Column(length = 1, name = "clube2_ponto")
	private int clube2Ponto;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "id_clube1", foreignKey = @ForeignKey(name = "fk_jogo_clube1"))
	private Clube clube1;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "id_clube2", foreignKey = @ForeignKey(name = "fk_jogo_clube2"))
	private Clube clube2;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "id_rodada", foreignKey = @ForeignKey(name = "fk_jogo_rodada"))
	private Rodada rodadaJogo;
	
}
