package br.edu.unisinos.bd2.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Posicao implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "s_Posicao")
	@SequenceGenerator(name = "s_Posicao", sequenceName = "s_Posicao", allocationSize = 1)
	@Column(name = "id")
	private Long idPosicao;
	
	@Column(length = 50, nullable = false)
	private String nome;
	
	@OneToMany(mappedBy = "posicao")
	private List<Jogador> jogadores;
	
}
