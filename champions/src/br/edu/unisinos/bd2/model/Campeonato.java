package br.edu.unisinos.bd2.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Campeonato implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "s_Campeonato")
	@SequenceGenerator(name = "s_Campeonato", sequenceName = "s_Campeonato", allocationSize = 1)
	@Column(name = "id")
	private Long idCampeonato;
	
	@Column(length = 50, nullable = false)
	private String nome;
	
	@Column(length = 4, nullable = false)
	private int ano;
	
	@Column(length = 2, nullable = false, name = "nro_clubes")
	private int nroClubes;
	
	@OneToMany(mappedBy = "campeonato")
	private List<Rodada> rodadas;
	
}
