package br.edu.unisinos.bd2.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Clube implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "s_Clube")
	@SequenceGenerator(name = "s_Clube", sequenceName = "s_Clube", allocationSize = 1)
	@Column(name = "id")
	private Long idClube;
	
	@Column(length = 50, nullable = false)
	private String nome;
	
	@Column(nullable = false, name = "dt_fundacao")
	private LocalDate dtFundacao;
	
	@OneToOne
	@JoinColumn(name = "id_tecnico", foreignKey = @ForeignKey(name = "fk_clube_tecnico"))
	private Tecnico tecnico;
	
	@OneToMany(mappedBy = "clube")
	private List<Jogador> jogadores;
	
	@OneToMany(mappedBy = "clube1")
	private List<Jogo> jogosMandante;
	
	@OneToMany(mappedBy = "clube2")
	private List<Jogo> jogosVisitante;
	
	@OneToMany(mappedBy = "clube")
	private List<ClassificacaoRodada> classificacaoRodadas;
	
}
